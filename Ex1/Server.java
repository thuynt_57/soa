

import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
        
public class Server implements Hello {
	private String[] sampleISBNs = {"978-0071605526", "978-0321815736", "978-0321718334"};
	private float[] samplePrices = {25, (float) 46.29, (float) 39.99};
	
	public String getISBN (int id) {
		return sampleISBNs[id];
	}
	
	public int sampleSize() {
		return samplePrices.length;
	}
	
	public float getPrice (int id) {
		return samplePrices[id];
	}
        
    public Server() {}

    public float getBookPrice(String ISND)  {
        for (int i = 0; i < sampleSize(); i++) {
        	if (getISBN(i).equals(ISND))
        		return getPrice(i);
        }
        return -2;
    }
        
    public static void main(String args[]) {
        try {
            Server obj = new Server();
            Hello stub = (Hello) UnicastRemoteObject.exportObject(obj, 0);

            // Bind the remote object's stub in the registry
            Registry registry = LocateRegistry.getRegistry();
            registry.bind("Hello", stub);

            System.err.println("Server ready");
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }
}
