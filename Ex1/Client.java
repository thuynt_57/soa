

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

public class Client {
    private Client() {};
    
    public String getISBNFromUser() {
    	System.out.print("Enter ISBN of the book: ");
    	Scanner keyboard = new Scanner(System.in);
    	return keyboard.next();
    	
    }
    
    public static void main(String[] args) {
            Client client = new Client();
    		Registry registry = null;
			try {
				registry = LocateRegistry.getRegistry();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            Hello stub = null;
			try {
				stub = (Hello) registry.lookup("Hello");
			} catch (RemoteException | NotBoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            float response = -1;
			try {
				response = stub.getBookPrice(client.getISBNFromUser());
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (response == -1 || response == -2) {
				System.out.println("We do not have this book, try 978-0071605526 or 978-0321815736 or 978-0321718334");
			} else {
            	System.out.println("The price of this book is: $" + response);
			}
   }
}

