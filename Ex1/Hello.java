

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Hello extends Remote {
    float getBookPrice(String ISND) throws RemoteException;
}