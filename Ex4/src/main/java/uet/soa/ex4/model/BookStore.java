package uet.soa.ex4.model;

import java.util.HashMap;
import java.util.Map;

public class BookStore {
	public static Map<String,Double> ISBN2Price;
	static {
		ISBN2Price = new HashMap<String,Double>();
		ISBN2Price.put("978-0071605526",25.00);
		ISBN2Price.put("978-0321815736",46.29);
		ISBN2Price.put("978-0321718334",39.99);
	}
	public static boolean isISBNExisted(String ISBN){
		return ISBN2Price.containsKey(ISBN);
	}
	public static double getPriceByISBN(String ISBN){
		return ISBN2Price.get(ISBN);
	}
	public BookStore(){		
	}
}
