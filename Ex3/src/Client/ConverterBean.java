package Client;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.lang.reflect.*;

import javax.faces.model.SelectItem;

import NET.webserviceX.www.Currency;

public class ConverterBean {
	/**
	 * Private variable declarations that are used to get and set currency and
	 * amount.
	 */
	private String fromCurrency = "USD";
	private String toCurrency = "VND";
	private double amount = 0.0;

	private double getCurrencyConversionRate(Currency fromCurrency, Currency toCurrency) throws RemoteException {
		NET.webserviceX.www.CurrencyConvertorSoapStub binding = null;
		try {
			binding = (NET.webserviceX.www.CurrencyConvertorSoapStub) new NET.webserviceX.www.CurrencyConvertorLocator()
					.getCurrencyConvertorSoap();
		} catch (javax.xml.rpc.ServiceException jre) {
			if (jre.getLinkedCause() != null)
				jre.getLinkedCause().printStackTrace();
		}

		// Time out after a minute
		binding.setTimeout(60000);
		return binding.conversionRate(fromCurrency, toCurrency);
	}

	/**
	 * Do something not-so-fancy here to create a currency.
	 * 
	 * @param currencyCode
	 * @return
	 */
	private Currency getCurrencyFromString(String currencyCode) {
		Currency c = Currency.fromString(currencyCode);
		return c;
	}

	public SelectItem[] getCurrencyList() {
		List<String> al = new ArrayList<String>();
		Field f[];

		try {
			Class c = Class.forName("NET.webserviceX.www.Currency");
			f = c.getFields();

			// Get the fields of the class that don't start with an "_",
			// because this is the list of currencies
			for (Field f1 : f) {
				if (!f1.getName().contains("_")) {
					al.add(f1.getName());
				}
			}
		} catch (ClassNotFoundException e) {
			System.out.println("Error getting currency list.");
		}
		Collections.sort(al);
		SelectItem[] items = new SelectItem[al.size()];
		for (int i = 0; i < al.size(); i++) {
			items[i] = new SelectItem(al.get(i).toString(), al.get(i).toString());
		}
		return items;
	}

	public String getFromCurrency() {
		return fromCurrency;
	}

	public void setFromCurrency(String fromCurrency) {
		this.fromCurrency = fromCurrency;
	}

	public String getToCurrency() {
		return toCurrency;
	}

	public void setToCurrency(String toCurrency) {
		this.toCurrency = toCurrency;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getConvertedAmount() throws RemoteException {
		Double convertedValue = 0.0;
		if (amount != 0)
			convertedValue = amount
					* getCurrencyConversionRate(getCurrencyFromString(fromCurrency), getCurrencyFromString(toCurrency));
		return convertedValue;
	}
}