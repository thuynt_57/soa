package Client;

import java.rmi.RemoteException;
import java.util.Currency;

public class MyConverter {
	public static double convertC(String from,String to,double value) throws RemoteException{
		ConverterBean converter = new ConverterBean();		
		converter.setFromCurrency(from);
		converter.setToCurrency(to);
		converter.setAmount(value);	
		return converter.getConvertedAmount();		
	}
	public static void main(String[] args) throws RemoteException{
		String from = args[0];
		String to = args[1];
		double value = Double.valueOf(args[2]);		
		System.out.println("From : "+value +" "+from +" to "+to+": "+convertC(from,to,value));		
	}
}
