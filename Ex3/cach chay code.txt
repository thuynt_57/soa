Move to folder Ex3/jar
Open command line and run following command:

java -classpath Ex3.jar Client.MyConverter "FROM" "TO" "VALUE"
Where as : 
- FROM : Original Currency , Ex : USD
- TO : Converted Currency, Ex: VND
- VALUE : the amount of money needed to convert. , Ex :1 